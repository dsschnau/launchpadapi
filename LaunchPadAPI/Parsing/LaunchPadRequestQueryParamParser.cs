﻿using SpaceXAPIDataAccess.Filtering;

namespace LaunchPadAPI.Parsing
{
    public class LaunchPadRequestQueryParamParser
    {
        public static LaunchPadQueryFilter Parse(string filterQuery)
        {
            if (string.IsNullOrWhiteSpace(filterQuery))
                return Default();

            bool includeId, includeName, includeStatus;
            includeId = includeName = includeStatus = false;
            var queries = filterQuery.ToLowerInvariant().Split(',');
            foreach(var query in queries)
            {
                switch(query)
                {
                    case "id":
                        includeId = true;
                        break;
                    case "name":
                        includeName = true;
                        break;
                    case "status":
                        includeStatus = true;
                        break;
                }
            }
            return new LaunchPadQueryFilter()
            {
                IncludeId     = includeId,
                IncludeName   = includeName,
                IncludeStatus = includeStatus
            };
        }

        private static LaunchPadQueryFilter Default()
        {
            return new LaunchPadQueryFilter()
            {
                IncludeId     = true,
                IncludeName   = true,
                IncludeStatus = true,
            };
        }
    }
}
