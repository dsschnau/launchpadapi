﻿using LaunchPadAPI.Parsing;
using Microsoft.AspNetCore.Mvc;
using SpaceXAPIDataAccess;
using System.Threading.Tasks;

namespace LaunchPadAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LaunchPadController : ControllerBase
    {
        public ILaunchPadService LaunchPadService { get; set; }

        public LaunchPadController(ILaunchPadService launchPadService)
        {
            this.LaunchPadService = launchPadService;
        }

        [HttpGet]
        public async Task<IActionResult> IndexAsync(string filter)
        {
            return Ok(
                await LaunchPadService.GetLaunchPadsAsync(LaunchPadRequestQueryParamParser.Parse(filter)));
        }
    }
}