﻿using Entities;
using SpaceXAPIDataAccess.Filtering;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpaceXAPIDataAccess
{
    public interface ILaunchPadService
    {
        Task<IEnumerable<LaunchPad>> GetLaunchPadsAsync(LaunchPadQueryFilter filter);
    }
}
