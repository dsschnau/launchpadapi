﻿using Entities;
using Newtonsoft.Json.Linq;

namespace SpaceXAPIDataAccess
{
    public class LaunchPadDataParser
    {
        public static LaunchPad Parse(JToken launchpadJson)
        {
            return new LaunchPad()
            {
               Id     = launchpadJson["id"].ToString(),
               Name   = launchpadJson["full_name"].ToString(),
               Status = launchpadJson["status"].ToString()
            };

        }
    }
}
