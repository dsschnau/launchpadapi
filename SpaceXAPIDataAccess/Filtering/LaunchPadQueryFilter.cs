﻿using Entities;
using System.Collections.Generic;
using System.Linq;

namespace SpaceXAPIDataAccess.Filtering
{
    public class LaunchPadQueryFilter
    {
        public bool IncludeId { get; set; }
        public bool IncludeName { get; set; }
        public bool IncludeStatus { get; set; }

        public IEnumerable<LaunchPad> Apply(IEnumerable<LaunchPad> launchPads)
        {
            return from launchPad in launchPads
                   select new LaunchPad()
                   {
                       Id = IncludeId ? launchPad.Id : null,
                       Name = IncludeName ? launchPad.Name : null,
                       Status = IncludeStatus ? launchPad.Status : null
                   };
        }
    }
}
