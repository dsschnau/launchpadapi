﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace SpaceXAPIDataAccess
{
    public class HttpClientProvider : IHttpClientProvider
    {
        private HttpClient _httpClient = new HttpClient();
        public HttpClient HttpClient => _httpClient;
    }
}
