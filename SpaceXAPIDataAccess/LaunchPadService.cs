﻿using Entities;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using SpaceXAPIDataAccess.Filtering;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SpaceXAPIDataAccess
{
    public class LaunchPadService : ILaunchPadService
    {
        public HttpClient HttpClient { get; private set; }
        public Uri SpacexDataUri { get; private set; }
        public ILogger Logger { get; private set; }

        public LaunchPadService(IHttpClientProvider httpClientProvider, Uri spaceXDataUri, ILoggerFactory loggerFactory)
        {
            this.HttpClient    = httpClientProvider.HttpClient;
            this.SpacexDataUri = spaceXDataUri;
            this.Logger        = loggerFactory.CreateLogger(typeof(LaunchPadService));
        }

        public async Task<IEnumerable<LaunchPad>> GetLaunchPadsAsync(LaunchPadQueryFilter filter)
        {
            try
            {
                var stopwatch         = Stopwatch.StartNew();
                var responseBody      = await HttpClient.GetStringAsync(SpacexDataUri);
                var launchPadJsonData = JArray.Parse(responseBody);
                var launchPads        = from json in launchPadJsonData select LaunchPadDataParser.Parse(json);
                Logger.LogInformation($"Retrieved spacex launchpad data in {stopwatch.ElapsedMilliseconds} milliseconds.");
                return filter.Apply(launchPads);
            }
            catch (HttpRequestException ex)
            {
                Logger.LogError(ex, "HttpRequest Exception retrieving spacex launchpad data.");
                return null;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Unhandled Exception retrieving spacex launchpad data.");
                return null;
            }
        }
    }
}
