﻿using System.Net.Http;

namespace SpaceXAPIDataAccess
{
    public interface IHttpClientProvider
    {
        HttpClient HttpClient { get; }
    }
}
