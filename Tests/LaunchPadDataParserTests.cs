using Newtonsoft.Json.Linq;
using SpaceXAPIDataAccess;
using Xunit;

namespace Tests
{
    public class LaunchPadDataParserTests
    {
        [Fact]
        public void LaunchPadDataParser_Parses()
        {
            var token = JToken.Parse(@"
            {
                'padid': 6,
                'id': 'vafb_slc_4e',
                'full_name': 'Vandenberg Air Force Base Space Launch Complex 4E',
                'status': 'active',
                'location': {
                        'name': 'Vandenberg Air Force Base',
                    'region': 'California',
                    'latitude': 34.632093,
                    'longitude': -120.610829
                },
                'vehicles_launched': [
                    'Falcon 9'
                ],
                'attempted_launches': 14,
                'successful_launches': 14,
                'wikipedia': 'https://en.wikipedia.org/wiki/Vandenberg_AFB_Space_Launch_Complex_4',
                'details': 'SpaceX primary west coast launch pad for polar orbits and sun synchronous orbits, primarily used for Iridium. Also intended to be capable of launching Falcon Heavy.'
            }");

            var result = LaunchPadDataParser.Parse(token);
            Assert.Equal("vafb_slc_4e", result.Id);
            Assert.Equal("Vandenberg Air Force Base Space Launch Complex 4E", result.Name);
            Assert.Equal("active", result.Status);
        }
    }
}
