﻿using LaunchPadAPI.Parsing;
using Xunit;

namespace Tests
{
    public class LaunchPadRequestQueryParamParserTests
    {
        [Fact]
        public void LaunchPadRequestQueryParamParser_Parses_Null_Filter()
        {
            var result = LaunchPadRequestQueryParamParser.Parse(null);
            Assert.True(result.IncludeId);
            Assert.True(result.IncludeName);
            Assert.True(result.IncludeStatus);
        }

        [Fact]
        public void LaunchPadRequestQueryParamParser_Parses_Partial_Filter()
        {
            var query = "id";
            var result = LaunchPadRequestQueryParamParser.Parse(query);
            Assert.True(result.IncludeId);
            Assert.False(result.IncludeName);
            Assert.False(result.IncludeStatus);
        }

        [Fact]
        public void LaunchPadRequestQueryParamParser_Parses_Filter_With_Garbage_Tokens()
        {
            var query = "name,qwerty";
            var result = LaunchPadRequestQueryParamParser.Parse(query);
            Assert.False(result.IncludeId);
            Assert.True(result.IncludeName);
            Assert.False(result.IncludeStatus);
        }

        [Fact]
        public void LaunchPadRequestQueryParamParser_Parses_Filter_With_Two_Tokens()
        {
            var query = "name,status";
            var result = LaunchPadRequestQueryParamParser.Parse(query);
            Assert.False(result.IncludeId);
            Assert.True(result.IncludeName);
            Assert.True(result.IncludeStatus);
        }

        [Fact]
        public void LaunchPadRequestQueryParamParser_Parses_Filter_With_All_Tokens()
        {
            var query = "id,name,status";
            var result = LaunchPadRequestQueryParamParser.Parse(query);
            Assert.True(result.IncludeId);
            Assert.True(result.IncludeName);
            Assert.True(result.IncludeStatus);
        }
    }
}
