﻿using System;

namespace Entities
{
    public class LaunchPad
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String Status { get; set; }
    }
}
