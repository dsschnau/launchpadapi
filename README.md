SpaceX Launchpad Data API Wrapper. Built on ASP.NET Core 2.1.

- endpoint is available at /api/launchpad.
- Query Filtering is enabled. Add a query paramter 'filter' to specify a comma-delimited list of fields you want to include. **e.g. `/api/launchpad?filter=name`** will provide only the names of launchpads.
- To Move to a different data store, implement a new `ILaunchPadService`, and configure the new implementation for dependency injection in `LaunchPadAPI.Startup.cs`.